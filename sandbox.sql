-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2015 at 02:41 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sandbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `lekarze`
--

CREATE TABLE IF NOT EXISTS `lekarze` (
  `id_lekarza` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `pass` text COLLATE utf8_polish_ci NOT NULL,
  `id_specjalnosc` int(10) unsigned NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_lekarza`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `lekarze`
--

INSERT INTO `lekarze` (`id_lekarza`, `imie`, `nazwisko`, `pass`, `id_specjalnosc`, `email`) VALUES
(1, 'Jan', 'Kruk', 'ea2b2676c28c0db26d39331a336c6b92', 1, 'jan.kruk@szpital.pl'),
(2, 'Anna', 'Jodko', 'ea2b2676c28c0db26d39331a336c6b92', 2, 'anna.jodko@szpital.pl'),
(3, 'Krystian', 'Mikołajczyk', 'ea2b2676c28c0db26d39331a336c6b92', 5, 'krystian.mikolajczyk@szpital.pl'),
(4, 'Zofia', 'Król', 'ea2b2676c28c0db26d39331a336c6b92', 4, 'zofia.krol@szpital.pl'),
(5, 'Jakub', 'Kowalski', 'ea2b2676c28c0db26d39331a336c6b92', 3, 'jakub.kowalski@szpital.pl');

-- --------------------------------------------------------

--
-- Table structure for table `pacjenci`
--

CREATE TABLE IF NOT EXISTS `pacjenci` (
  `pesel` bigint(36) unsigned NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `telefon` text COLLATE utf8_polish_ci NOT NULL,
  `pass` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`pesel`),
  UNIQUE KEY `pesel` (`pesel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `pacjenci`
--

INSERT INTO `pacjenci` (`pesel`, `imie`, `nazwisko`, `telefon`, `pass`, `email`) VALUES
(123456789, 'sadsd', 'sdsadasd', 'dsadsd', 'ea2b2676c28c0db26d39331a336c6b92', 'a@2oo22o.pl'),
(1212121212, 'Krystyna', 'Czubówna', '+48666666666', 'ea2b2676c28c0db26d39331a336c6b92', 'krystyna.czubowna@mail.pl'),
(65101569274, 'Katarzyna', 'Milewska', '666-565-214', '697f36a96ac499ffa33090462685529a', 'katarzyna.milewska@mail.pl'),
(70140265782, 'Piotr', 'Potoczny', '693-567-677', '2f6248d1fe2039898905507499c14458', 'piotr.potoczny@mail.pl'),
(71090869735, 'Jakub', 'Czarnecki', '700-543-665', 'ea2b2676c28c0db26d39331a336c6b92', 'jakub.czarnecki@mail.pl'),
(85061782395, 'Patrycja', 'Pawlak', '676-565-454', '480e85de41e38f219d486628b23ed5eb', 'patrycja.pawlak@mail.pl'),
(89052667564, 'Filip', 'Piotrowski', '654-654-345', 'd3229d970174955c47b80d4f551dd38a', 'filip.piotrowski@mail.pl'),
(90081269785, 'Krystyna', 'Czeledzka', '666-778-435', '5e423ac7f043111c39463c49d16a774f', 'krystyna.czeladzka@mail.pl'),
(92030312456, 'Jan', 'Kowalski', '+48606202303', 'ea2b2676c28c0db26d39331a336c6b92', 'jan.kowalski@mail.pl'),
(92042312345, 'Wojciech', 'Czerniecki', '693-424-565', 'ea2b2676c28c0db26d39331a336c6b92', 'czerniecki.wojciech@gmail.com'),
(92051720476, 'Bartłomiej', 'Zen', '666-565-888', '2fe3246c59a1778b74b0372d1506448a', 'bartlomiej.zen@mail.pl'),
(94120445769, 'Anna', 'Mucha', '567-654-455', '9c1fdf887192f9dfb334e7ca292f4de3', 'anna.much@mail.pl');

-- --------------------------------------------------------

--
-- Table structure for table `specjalnosci`
--

CREATE TABLE IF NOT EXISTS `specjalnosci` (
  `id_specjalnosc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_specjalnosc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `specjalnosci`
--

INSERT INTO `specjalnosci` (`id_specjalnosc`, `nazwa`) VALUES
(1, 'Pediatra'),
(2, 'Stomatolog'),
(3, 'Dermatolog'),
(4, 'Ginekolog'),
(5, 'Ortodonta'),
(6, 'Okulista');

-- --------------------------------------------------------

--
-- Table structure for table `wizyty`
--

CREATE TABLE IF NOT EXISTS `wizyty` (
  `id_wizyty` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pesel` bigint(20) unsigned NOT NULL,
  `id_lekarz` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `godzina` int(11) NOT NULL,
  PRIMARY KEY (`id_wizyty`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=102 ;

--
-- Dumping data for table `wizyty`
--

INSERT INTO `wizyty` (`id_wizyty`, `pesel`, `id_lekarz`, `data`, `godzina`) VALUES
(1, 0, 1, '2015-06-24', 13),
(3, 65101569274, 2, '2015-06-11', 14),
(4, 0, 1, '2015-06-26', 14),
(18, 0, 1, '2015-06-15', 8),
(19, 92042312345, 1, '2015-06-15', 9),
(20, 0, 1, '2015-06-15', 10),
(21, 0, 1, '2015-06-15', 11),
(22, 0, 1, '2015-06-15', 14),
(23, 0, 1, '2015-06-15', 15),
(24, 0, 1, '2015-06-15', 16),
(25, 0, 1, '2015-06-15', 12),
(26, 0, 1, '2015-06-15', 13),
(27, 0, 1, '2015-06-15', 17),
(28, 0, 1, '2015-06-15', 18),
(29, 0, 1, '2015-06-15', 19),
(30, 0, 1, '2015-06-17', 7),
(31, 0, 1, '2015-06-16', 7),
(32, 0, 1, '2015-06-16', 8),
(33, 0, 1, '2015-06-16', 9),
(34, 0, 1, '2015-06-16', 10),
(35, 0, 1, '2015-06-16', 11),
(36, 0, 1, '2015-06-18', 7),
(37, 0, 1, '2015-06-18', 8),
(38, 0, 1, '2015-06-18', 9),
(39, 0, 1, '2015-06-18', 10),
(40, 0, 1, '2015-06-18', 11),
(41, 0, 1, '2015-06-18', 12),
(42, 0, 1, '2015-06-18', 13),
(43, 0, 1, '2015-06-18', 14),
(44, 0, 1, '2015-06-18', 15),
(45, 0, 1, '2015-06-18', 16),
(46, 0, 1, '2015-06-18', 17),
(47, 0, 1, '2015-06-18', 18),
(48, 0, 1, '2015-06-16', 12),
(49, 0, 1, '2015-06-16', 13),
(50, 0, 1, '2015-06-16', 14),
(51, 0, 1, '2015-06-16', 15),
(52, 0, 1, '2015-06-16', 16),
(53, 0, 1, '2015-06-16', 17),
(54, 0, 1, '2015-06-16', 18),
(55, 0, 1, '2015-06-16', 19),
(56, 0, 2, '2015-06-16', 7),
(57, 0, 2, '2015-06-16', 8),
(58, 0, 2, '2015-06-16', 9),
(59, 0, 2, '2015-06-16', 10),
(60, 92042312345, 2, '2015-06-16', 11),
(61, 0, 2, '2015-06-16', 12),
(62, 0, 2, '2015-06-16', 13),
(63, 0, 2, '2015-06-16', 14),
(64, 0, 2, '2015-06-16', 15),
(65, 0, 2, '2015-06-16', 16),
(66, 0, 2, '2015-06-16', 17),
(67, 0, 2, '2015-06-16', 18),
(68, 0, 3, '2015-06-17', 7),
(69, 0, 3, '2015-06-17', 8),
(70, 92042312345, 3, '2015-06-17', 9),
(71, 0, 3, '2015-06-17', 10),
(72, 0, 3, '2015-06-17', 11),
(73, 0, 3, '2015-06-17', 12),
(74, 0, 3, '2015-06-17', 13),
(75, 0, 3, '2015-06-17', 14),
(76, 0, 3, '2015-06-17', 15),
(77, 0, 3, '2015-06-17', 16),
(78, 0, 3, '2015-06-17', 17),
(79, 0, 3, '2015-06-17', 18),
(80, 0, 3, '2015-06-17', 19),
(81, 92042312345, 4, '2015-06-28', 7),
(82, 0, 4, '2015-06-28', 8),
(83, 0, 4, '2015-06-28', 9),
(84, 0, 4, '2015-06-28', 10),
(85, 0, 4, '2015-06-28', 11),
(86, 0, 4, '2015-06-28', 12),
(87, 0, 4, '2015-06-28', 13),
(88, 0, 4, '2015-06-28', 14),
(89, 0, 5, '2015-07-14', 7),
(90, 0, 5, '2015-07-14', 8),
(91, 0, 5, '2015-07-14', 9),
(92, 0, 5, '2015-07-14', 10),
(93, 0, 5, '2015-07-14', 11),
(94, 0, 5, '2015-07-14', 12),
(95, 0, 5, '2015-07-14', 13),
(96, 92042312345, 5, '2015-07-14', 14),
(97, 0, 5, '2015-07-14', 15),
(98, 0, 5, '2015-07-14', 16),
(99, 0, 5, '2015-07-14', 17),
(100, 0, 5, '2015-07-14', 18),
(101, 0, 5, '2015-07-14', 19);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
