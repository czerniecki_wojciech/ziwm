#!/usr/bin/env/python
import web
import os
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class pliki:
	def GET(self, prefix, file_name):
		# print("Getting file " + file_name)
		file = open(os.getcwd() + "/templates/pliki/" + file_name, 'r')
		return file.read()
