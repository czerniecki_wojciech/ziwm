#!/usr/bin/env/python
import web
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')
class notFound:
	def GET(self, prefix, name):
		print("not found: " + name + " in " + prefix)
		return render.index('not_found')
