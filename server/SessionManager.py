#!/usr/bin/env/python

import uuid

class SessionManagerImpl:
	def __init__(self):
		print "SessionManager has been created"
		self.sessions = {}

	def getId(self):
		return str(uuid.uuid4())

	def createNewSession(self):
		sessionId = self.getId()
		self.sessions[sessionId] = 1
		print self.sessions
		return sessionId

	def getSessionData(self, sessionId):
		try:
			return self.sessions[sessionId]
		except KeyError:
			print "There is no suitable session"
			return False

	def deleteSession(self, sessionId):
		if sessionId in self.sessions:
			del self.sessions[sessionId]

	def setSessionData(self, sessionId, newValue):
		if sessionId in self.sessions:
			self.sessions[sessionId] = newValue

class SessionManager:
	instance = SessionManagerImpl()

	def get(self):
		return self.instance