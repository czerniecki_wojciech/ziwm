#!/usr/bin/env/python
import web
from SessionManager import SessionManager
from index import index
from mojeWizyty import mojeWizyty
from dodajDyzury import dodajDyzury
from logowanieLekarz import logowanieLekarz
from logowaniePacjent import logowaniePacjent
from mojeDyzury import mojeDyzury
from zarezerwujWizyte import zarezerwujWizyte
from pliki import pliki
from notFound import notFound
from wyloguj import wyloguj
from odwolajWizyte import odwolajWizyte
from zarejestruj import zarejestruj

render = web.template.render('templates/')

db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

sessionChecker = SessionManager()

urls = (
	'(.*)/pliki/(.*)', pliki,
	'(.*)/moje_wizyty', mojeWizyty,
	'(.*)/moje_wizyty/(.*)', mojeWizyty,
	'(.*)/zarezerwuj_wizyte', zarezerwujWizyte,
	'(.*)/odwolaj_wizyte/(.*)', odwolajWizyte,
	'(.*)/zarezerwuj_wizyte/(.*)', zarezerwujWizyte,
	'(.*)/dodaj_dyzury', dodajDyzury,
	'(.*)/dodaj_dyzury/(.*)', dodajDyzury,
	'/zarejestruj', zarejestruj,
	'/zarejestruj/(.*)', zarejestruj,
	'/logowanie', logowaniePacjent,
	'/logowanie/(.*)', logowaniePacjent,
	'/logowanie_lekarz', logowanieLekarz,
	'/logowanie_lekarz/(.*)', logowanieLekarz,
	'(.*)/wyloguj', wyloguj,
	'(.*)/moje_dyzury', mojeDyzury,
	'(.*)/moje_dyzury/(.*)', mojeDyzury,
	'(.*)/', index,
	'(.*)/(.*)', notFound
)

# get = web.input()
# if hasattr(get, 'name'):
# 	return render.admin(get.name)

if __name__ == "__main__":
	app = web.application(urls, globals())
	app.run()
