#!/usr/bin/env/python
# -*- coding: utf-8 -*-
import web
from datetime import datetime
from SessionManager import SessionManager
render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class odwolajWizyte:
    def GET(self, sid, params):
        isADoctor = None
        id_wizyty = None

        params_num = str(params).count("/") + 1

        isADoctor, id_wizyty = str(params).split("/")

        if isADoctor == '1':
            isADoctor = True
        else:
            isADoctor = False

        if isADoctor is True:
            session_checker = SessionManager().get()
            id_lekarz = str(session_checker.getSessionData(sid[1:]))
            check = db.query("SELECT wizyty.pesel FROM wizyty WHERE wizyty.id_wizyty = " + id_wizyty + " AND wizyty.id_lekarz = " + id_lekarz)

            if len(check) == 1:
                pesel = str(check[0].pesel)
                if str(pesel) != '0':
                    web.config.smtp_server = 'smtp.gmail.com'
                    web.config.smtp_port = 587
                    web.config.smtp_username = 'przychodnia.doktorek@gmail.com'
                    web.config.smtp_password = 'doktorek123'
                    web.config.smtp_starttls = True

                    email = db.query("SELECT pacjenci.email FROM pacjenci WHERE pacjenci.pesel = " + pesel)

                    subject = "Dyżur lekarza został odwołany"
                    message = "Witaj drogi pacjencie przychodni Doktorek. Jest nam niezmiernia przykro, ale lekarz odwołał zaplanowany dyżur. Prosimy zarezerwować inny termin. Z wyazami szacunku, Przychodnia Doktorek"

                    web.sendmail('przychodnia.doktorek@gmail.com', email[0].email, subject, message)

                db.query("DELETE FROM wizyty WHERE wizyty.id_wizyty = " + id_wizyty)
                web.seeother("../../../" + sid[1:] + "/dodaj_dyzury/6")
            else:
                web.seeother("../../../" + sid[1:] + "/dodaj_dyzury/5")
        else:
            session_checker = SessionManager().get()
            pesel = str(session_checker.getSessionData(sid[1:]))

            check = db.query("SELECT wizyty.id_wizyty FROM wizyty WHERE wizyty.id_wizyty = " + id_wizyty + " AND wizyty.pesel = " + pesel)

            if len(check) == 1:
                db.query("UPDATE wizyty SET pesel = '0' WHERE wizyty.id_wizyty = " + id_wizyty)
                web.seeother("../../../" + sid[1:] + "/moje_wizyty/1/2")
            else:
                web.seeother("../../../" + sid[1:] + "/moje_wizyty/1/3")
