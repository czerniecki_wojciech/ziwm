#!/usr/bin/env/python
import web
from datetime import datetime
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class mojeWizyty:
	def getCurrentDate(self):
		now = datetime.now()
		date = str(now.year) + '-'

		if now.month < 10:
			date += '0'
		date += str(now.month) + '-'

		if now.day < 10:
			date += '0'
		date += str(now.day)

		return date

	def getVisit(self, sid, page):
		session_checker = SessionManager().get()
		id_pacjenta = str(session_checker.getSessionData(sid))

		now = self.getCurrentDate()

		return db.query('SELECT wizyty.data AS data,'
						' wizyty.godzina AS godzina,'
						' wizyty.pesel AS pesel,'
						'wizyty.id_wizyty AS id_wizyty,'
						' CONCAT (lekarze.imie, " " , lekarze.nazwisko) AS imie '
						'FROM `wizyty` '
						'JOIN lekarze on wizyty.id_lekarz = lekarze.id_lekarza '
						'WHERE pesel = ' + id_pacjenta +
						' AND DATE(wizyty.data) >= DATE("' + now + '") ORDER BY wizyty.data ASC, wizyty.godzina ASC '
						'LIMIT ' + str((page - 1) * 10) + ', ' + str(page * 10))

	def getTotalVisitsNum(self, sid):
		session_checker = SessionManager().get()
		id_pacjenta = str(session_checker.getSessionData(sid))
		result = db.query("SELECT COUNT(wizyty.id_wizyty) AS total FROM wizyty WHERE wizyty.pesel = " + id_pacjenta)
		return int(result[0].total)

	def GET(self, sid, params = ''):
		uri_prefix = None
		page = None
		info = None

		if params == '':
			return web.seeother("./moje_wizyty/1")

		params_num = str(params).count("/") + 1

		if params_num == 2:
			page, info = str(params).split("/")
		elif params_num == 1:
			page = params

		if info is None:
			uri_prefix = '..'
		else:
			uri_prefix = '../..'
			print "info: " + info

		visit = self.getVisit(sid[1:], int(page))
		next_allowed = False

		if self.getTotalVisitsNum(sid[1:]) > 10 * int(page):
			next_allowed = True

		return render.moje_wizyty(visit, int(page), info = info, next_allowed = next_allowed, uri_prefix=uri_prefix)
