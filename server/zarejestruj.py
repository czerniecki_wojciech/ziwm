import web
from datetime import datetime
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class zarejestruj:
    def POST(self):
        wd = web.input()
        imie = wd.imie
        nazwisko = wd.nazwisko
        pesel = wd.pesel
        haslo1 = wd.password
        haslo2 = wd.password2
        telefon = wd.telefon
        email = wd.email

        if haslo1 != haslo2:
            return web.seeother('/zarejestruj/2')

        userExist = db.query('SELECT * FROM pacjenci WHERE pacjenci.pesel = ' + pesel)

        if len(userExist) == 1:
            return web.seeother('/zarejestruj/1')

        db.query("INSERT INTO pacjenci (pesel, imie, nazwisko, telefon, pass, email) VALUES ('" + pesel + "', '" + imie + "', '" + nazwisko + "', '" + telefon + "', MD5('" + haslo1 + "'), '" + email + "')")

        return web.seeother('/zarejestruj/0')

    def GET(self, info = ''):
        uri_prefix = '.'
        if info != '':
            uri_prefix = '..'
        return render.zarejestruj(info = info, uri_prefix = uri_prefix)
