#!/usr/bin/env/python
import web
from datetime import datetime
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class wyloguj:
    def GET(self, sid):
        session_checker = SessionManager().get()
        print "deleting session " + sid[1:]
        session_checker.deleteSession(sid[1:])
        web.seeother("/")
