#!/usr/bin/env/python
import web
from datetime import datetime
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class dodajDyzury:
	def POST(self, sid):
		session_checker = SessionManager().get()
		lekarz_id = str(session_checker.getSessionData(sid[1:]))
		comment = 4

		print lekarz_id

		if lekarz_id != 'False':
			web_data = web.input()
			hasBeenAdded = False
			everyAdded = True

			if hasattr(web_data, 'date') and hasattr(web_data, 'start_time') and hasattr(web_data, 'end_time'):
				print web_data.date + " " + web_data.start_time + " " + web_data.end_time
				date = datetime.strptime(web_data.date, "%d-%m-%Y").strftime("%Y-%m-%d")

				for time in range(int(web_data.start_time), int(web_data.end_time)):
					found = db.query('SELECT wizyty.id_wizyty FROM wizyty WHERE wizyty.id_lekarz = ' + str(lekarz_id) + ' '
							'AND DATE(wizyty.data) = DATE("' + date + '") '
							'AND wizyty.godzina = ' + str(time) + ' LIMIT 0 , 1')

					if len(found) == 0:
						hasBeenAdded = True
						db.query("INSERT INTO `sandbox`.`wizyty` (`id_wizyty`, `pesel`, `id_lekarz`, `data`, `godzina`) "
							 "VALUES (NULL, 0, '" + lekarz_id + "', '" + date + "', '" + str(time) + "')")
					else:
						everyAdded = False

			if hasBeenAdded is False:
				comment = 3
			elif hasBeenAdded is True and everyAdded is True:
				comment = 1
			elif hasBeenAdded is True and everyAdded is False:
				comment = 2

		web.seeother("../" + sid[1:] + "/dodaj_dyzury/" + str(comment))

	def GET(self, sid, params = ''):
		uri_prefix = '.'
		comment = 0

		if params != '':
			uri_prefix = '..'
			comment = int(params)

		return render.dodaj_dyzury(comment = comment, uri_prefix = uri_prefix)
