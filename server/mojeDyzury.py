#!/usr/bin/env/python
import web
from datetime import datetime
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class mojeDyzury:
	def getCurrentDate(self):
		now = datetime.now()
		date = str(now.year) + '-'

		if now.month < 10:
			date += '0'
		date += str(now.month) + '-'

		if now.day < 10:
			date += '0'
		date += str(now.day)

		return date

	def getDuty(self, id_lekarza, page):
		now = self.getCurrentDate()

		return db.query('SELECT lekarze.id_lekarza AS id_lekarza,'
						' wizyty.data AS data,'
						' wizyty.godzina AS godzina,'
						'wizyty.id_wizyty AS id_wizyty,'
						' CONCAT (pacjenci.imie, " ", pacjenci.nazwisko) AS imie '
						'FROM lekarze '
						'JOIN wizyty on lekarze.id_lekarza = wizyty.id_lekarz '
						'JOIN pacjenci on wizyty.pesel = pacjenci.pesel '
						'WHERE lekarze.id_lekarza = ' + id_lekarza +
						' AND DATE(wizyty.data) >= DATE("' + now + '") '
						'LIMIT ' + str((page - 1) * 10) + ', ' + str(page * 10))

	def getTotalDutyNum(self, sid):
		session_checker = SessionManager().get()
		id_lekarz = str(session_checker.getSessionData(sid))
		result = db.query("SELECT COUNT(wizyty.id_wizyty) AS total FROM wizyty WHERE wizyty.id_lekarz = " + id_lekarz)
		return int(result[0].total)

	def GET(self, sid, page = ''):
		uri_prefix = None

		if page == '':
			page = 1
			uri_prefix = '.'
		else:
			uri_prefix = '..'

		session_checker = SessionManager().get()
		id_lekarza = str(session_checker.getSessionData(sid[1:]))

		duty = self.getDuty(id_lekarza, int(page))
		next_allowed = False

		if self.getTotalDutyNum(sid[1:]) > 10 * int(page):
			next_allowed = True

		return render.moje_dyzury(duty, int(page), next_allowed = next_allowed, uri_prefix=uri_prefix)
