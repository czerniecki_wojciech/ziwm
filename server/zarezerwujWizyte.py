#!/usr/bin/env/python
import web
from SessionManager import SessionManager

render = web.template.render('templates/')
db = web.database(dbn='mysql', user='root', pw='start', db='sandbox')

class zarezerwujWizyte:
	def getSpecializations(self):
		return db.select('specjalnosci')

	def getDoctors(self, id_specjalnosc):
		return db.query('SELECT * FROM lekarze WHERE lekarze.id_specjalnosc = ' + id_specjalnosc)

	def getVisits(self, id_lekarza):
		return db.query("SELECT wizyty.id_wizyty AS id, wizyty.data AS data, wizyty.godzina AS godzina FROM wizyty join lekarze on wizyty.id_lekarz = lekarze.id_lekarza WHERE pesel = 0 AND wizyty.id_lekarz = " + str(id_lekarza) + " ORDER BY wizyty.data ASC, wizyty.godzina ASC")

	def getUriPrefix(self, id_specjalnosc, id_lekarza, page):
		if page is not None:
			return "../../.."
		elif id_lekarza is not None:
			return "../.."
		elif id_specjalnosc is not None:
			return ".."
		else:
			return "."

	def GET(self, sid, params = None):
		id_specjalnosc = None
		id_lekarza = None
		page = None
		id_wizyty = None

		params_num = str(params).count("/") + 1

		if params_num == 4:
			id_specjalnosc, id_lekarza, page, id_wizyty = str(params).split("/")
		elif params_num == 3:
			id_specjalnosc, id_lekarza, page = str(params).split("/")
		elif params_num == 2:
			id_specjalnosc, id_lekarza = str(params).split("/")
		elif params_num == 1:
			id_specjalnosc = params

		uri_prefix = self.getUriPrefix(id_specjalnosc, id_lekarza, page)

		if id_specjalnosc is None:  # before specialization select
			specializations = self.getSpecializations()
			return render.zarezerwuj_wizyte(specializations, uri_prefix=uri_prefix)
		elif id_lekarza is None:  # before doctor select
			specializations = self.getSpecializations()
			doctors = self.getDoctors(id_specjalnosc)
			return render.zarezerwuj_wizyte(specializations, long(id_specjalnosc), doctors, uri_prefix=uri_prefix)
		elif page is None:  # if no page is set
			web.seeother("/" + sid[1:] + "/zarezerwuj_wizyte/" + str(id_specjalnosc) + "/" + str(id_lekarza) + "/1")
		elif id_wizyty is None:  # before processing request
			specializations = self.getSpecializations()
			doctors = self.getDoctors(id_specjalnosc)
			visits = self.getVisits(id_lekarza)
			return render.zarezerwuj_wizyte(specializations, long(id_specjalnosc), doctors, id_lekarza, visits, uri_prefix=uri_prefix)
		else:  # processing request
			query = db.query('SELECT wizyty.pesel FROM wizyty WHERE wizyty.id_wizyty = ' + str(id_wizyty))
			session_checker = SessionManager().get()
			pesel = str(session_checker.getSessionData(sid[1:]))

			isSuccess = 0
			if query[0].pesel == 0:
					db.query('UPDATE sandbox.wizyty SET pesel = ' + pesel + ' WHERE wizyty.id_wizyty = ' + id_wizyty)
					isSuccess = 1

			web.seeother("../../../../../" + sid[1:] + "/moje_wizyty/1/" + str(isSuccess))
